import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt'
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { User } from '../users/entities/user.entity';

@Injectable()
export class AuthService {
  constructor(
    private userService: UsersService,
    private jwtService: JwtService
  ) { }

  async validateUser(email: string, password: string) {
    const user = await this.userService.findByEmail(email)
    if (!user) return null
    const passwordMatched = await bcrypt.compare(password, user.password)
    return passwordMatched ? user : null
  }

  async login(user: User) {
    const payload = { id: user.id, name: user.name, email: user.email, role: user.role }
    return {
      access_token: this.jwtService.sign(payload, { expiresIn: '15m', secret: process.env.JWT_SECRET })
    }
  }

  async updatePassword(id: number, password: string) {
    const hashedPassword = await bcrypt.hash(password, 10)
    await this.userService.update(id, { password: hashedPassword })
    return { success: true }
  }
}
