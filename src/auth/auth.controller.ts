import { Body, Controller, Post, Put, Request, UnauthorizedException, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { UpdatePasswordDto } from '../users/dto/update-password.dto';
import { AuthService } from './auth.service';
import { LoginDto } from './dto/login.dto';
import { JwtAuthGuard } from './jwt-auth.guard';

@ApiTags('auth')
@Controller('api')
export class AuthController {
  constructor(
    private readonly authService: AuthService
  ) { }

  @Post('login')
  @ApiOperation({ summary: 'login' })
  async login(@Body() loginDto: LoginDto) {
    const user = await this.authService.validateUser(loginDto.email, loginDto.password)

    if (!user) {
      throw new UnauthorizedException()
    }

    return this.authService.login(user)
  }

  @UseGuards(JwtAuthGuard)
  @Put('password')
  @ApiOperation({ summary: 'update password' })
  @ApiBearerAuth()
  updatePassword(@Request() req: any, @Body() body: UpdatePasswordDto) {
    return this.authService.updatePassword(req.user.id, body.password)
  }
}
