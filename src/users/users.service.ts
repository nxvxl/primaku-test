import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User, UserRole } from './entities/user.entity';
import * as bcrypt from 'bcrypt'

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>
  ) {
    this.usersRepository.find()
      .then(async users => {
        if (users.length === 0) {
          const hashedPassword = await bcrypt.hash('#Admin123', 10)
          await this.usersRepository.save({
            name: 'admin',
            email: 'admin@primaku.com',
            role: UserRole.admin,
            password: hashedPassword
          })
        }
      })
  }

  async create(createUserDto: CreateUserDto) {
    const { name, email, role } = createUserDto
    const user = await this.usersRepository.findOneBy({ email })

    if (user) {
      return null
    }

    const password = await bcrypt.hash(createUserDto.password, 10)
    return this.usersRepository.save({ name, email, role, password })
  }

  findAll(): Promise<User[]> {
    return this.usersRepository.find()
  }

  findOne(id: number) {
    return this.usersRepository.findOneBy({ id: id })
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    return this.usersRepository.update({ id: id }, updateUserDto)
  }

  remove(id: number) {
    return this.usersRepository.delete({ id: id })
  }

  findByEmail(email: string) {
    return this.usersRepository.findOneBy({ email: email })
  }
}
