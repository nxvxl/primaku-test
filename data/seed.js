require('dotenv').config()

import mysql from 'mysql2'

const {
  DB_HOST,
  DB_PORT,
  DB_NAME,
  DB_USER,
  DB_PASS
} = process.env

const conn = mysql.createConnection({
  host: DB_HOST,
  port: DB_PORT,
  user: DB_USER,
  password: DB_PASS,
  database: DB_NAME
})

const query = `CREATE DATABASE IF NOT EXISTS ${DB_NAME};
CREATE TABLE IF NOT EXISTS user;
INSERT INTO user (id, )`
conn.query()
